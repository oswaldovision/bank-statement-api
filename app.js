require('./config/config')
const app = require('express')()
const bodyParser = require('body-parser')
const {upload} = require('./multerSettings')
const db = require('./db/db')
const fileRoute = require('./routes/uploadFile')
const dataStatementRoute = require('./routes/data-Bank-Statement')
const fileStatement = require('./routes/file-Bank-Statement')
const cors = require('cors')

const port = process.env.PORT

app.set('view engine', 'ejs')
app.use(bodyParser.json())
app.use(cors())

app.get('/', (req, res) => {
  res.render('index')
})

app.use('/file', upload.single('file'), fileRoute)
app.use('/statement', dataStatementRoute)
app.use('/pdf', fileStatement)

db.connect(process.env['MONGODB_URI'], process.env['DATABASE_NAME'], function (err) {
  if (err) {
    console.log('Unable to connect to Mongo.')
    process.exit(1)
  } else {
    app.listen(port, () => {
      console.log('Server started on port: ' + port)
    })
  }
})

