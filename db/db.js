const MongoClient = require('mongodb').MongoClient

const state = {
  db: null,
}

exports.connect = function (url, database, done) {
  if (state.db) return done()

  const client = new MongoClient(url, {
    useNewUrlParser: true
  })

  client.connect(function (err) {
    if (err) return done(err)
    console.log('Connected correctly to server MongoDb')
    state.db = client.db(database)
    done()
  })

}

exports.get = function () {
  return state.db
}

exports.close = function (done) {
  if (state.db) {
    state.db.close(function (err, result) {
      state.db = null
      state.mode = null
      done(err)
    })
  }
}