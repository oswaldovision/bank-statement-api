const db = require('./db')

const statement = {

  insertData (data) {
    db.get().collection('statements').insertOne(data, (err, result) => {
      if (err) throw new Error(`Fail insert statement ${data.codeClient}`)

      console.log('Document insert: ' + result.insertedId)
    })
  },

  insertBuffer (img) {
    db.get().collection('bufferStatements').insertOne(img, (err, result) => {
      if (err) throw new Error(`Fail insert buffer image`)

      console.log('Document insert: ' + result.insertedId)
    })
  },

  getImages (req, res) {
    return db.get().collection('bufferStatements').aggregate([
      {$match: {}},
      {
        $project: {
          'createDate': 1,
          'typeProduct': 1,
          'ownerAccount': 1
        }
      },
      {$skip: 20},
      {$limit: 100}
    ]).toArray((err, list) => {
      if (err) throw new Error(`Fail find images`)

      res.status(200).send(list)
    })
  },

  downloadStatementById (req, res) {
    const ObjectId = require('mongodb').ObjectID
    return db.get().collection('bufferStatements').findOne({'_id': new ObjectId(req.params.id)}, (error, data) => {
      if (error) throw new Error(`Fail insert buffer image`)

      if (data) {
        res.writeHead(200, {
          'Content-Type': 'application/pdf',
          'Content-Disposition': 'attachment; filename=' + data.ownerAccount,
          'Content-Length': data.img.data.data.length
        })
        res.end(new Buffer(data.img.data.data, 'binary'))
      }

      res.status(404).send({message: 'File not found'})

    })
  }
}

module.exports = statement