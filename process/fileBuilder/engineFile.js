const {fork} = require('child_process')
const {sendMail} = require('./../mailing/index')
const {insertBuffer} = require('./../../db/statement')

const csmForked = fork('./process/fileBuilder/pdf')
const tcForked = fork('./process/fileBuilder/pdf')
const pacForked = fork('./process/fileBuilder/pdf')

csmForked.on('message', data => {
  saveDataFile(data)
  mailingData(data)
})

tcForked.on('message', data => {
  saveDataFile(data)
  mailingData(data)
})

pacForked.on('message', data => {
  saveDataFile(data)
  mailingData(data)
})

const saveDataFile = (data) => {
  const document = {
    img: data.buffer,
    contentType: 'image/pdf',
    insertDate: Date.now().toString(),
    typeProduct: data.typeProduct,
    ownerAccount: data.ownerAccount,
    account: data.account,
    customerEmail: data.customerEmail,
    batch: data.batch
  }
  insertBuffer(document)
}

const mailingData = (data) => {
  let base64String = Buffer.from(data.buffer.data).toString('base64')
  //TODO: Change prop 'to' -> after tests
  const settings = {
    to: ['oswaldom@visionsoftware.com.co'],// data.customerEmail
    subject: `(Prueba Azure SendGrid) Falabella extracto bancario: ${data.ownerAccount} ${data.typeProduct}`,
    attachments: [
      {
        content: base64String,
        filename: `${data.ownerAccount} - ${data.typeProduct}`,
        type: 'application/pdf',
        disposition: 'attachment'
      },
    ]
  }

  sendMail(settings)
}

const buildSaveMailing = (data, template, mapFunc, forked) => {
  data = mapFunc(data)
  forked.send({data, template})
}

module.exports = {buildSaveMailing, csmForked, tcForked, pacForked}