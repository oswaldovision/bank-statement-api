const Fs = require('fs')
const Handlebars = require('handlebars')
const Util = require('util')
const Puppeteer = require('puppeteer')
const Path = require('path')
const crypto = require('crypto')
const ReadFile = Util.promisify(Fs.readFile)

class Pdf {
  constructor (d, t) {
    this.data = d
    this.template = t
  }

  async html () {
    try {

      const templatePath = Path.resolve('assets', 'templates', this.template)
      const content = await ReadFile(templatePath, 'utf8')

      // compile and render the template with handlebars
      const template = Handlebars.compile(content)

      return template(this.data)
    } catch (error) {
      throw new Error('Cannot create invoice HTML template.')
    }
  }

  async pdf () {
    // const templateCss = Path.resolve('assets', 'templates', 'csm-style.css')

    const html = await this.html()

    const browser = await Puppeteer.launch({args: ['--no-sandbox']})
    const page = await browser.newPage()
    // await page.addStyleTag({path: templateCss})
    await page.setViewport({width: 761, height: 800})

    await page.emulateMedia('screen')
    await page.setContent(html)

    // TODO: Write File 1
    // const fileName = crypto.randomBytes(16).toString('hex')
    // Fs.writeFileSync(Path.resolve('output', `${fileName}.pdf`), '')

    return page.pdf({
        // path: Path.resolve('output', `${fileName}.pdf`),// TODO: Write File 2
        format: 'A4',
        margin: {top: '50px', right: '10px', bottom: '50px', left: '10px',}
      }
    )
  }
}

process.on('message', (msg) => {

  const pdf = new Pdf(msg.data, msg.template)
  pdf.html()

  pdf.pdf()
    .then(file => {
      process.send({
        buffer: file,
        typeProduct: pdf.data.tipo_producto,
        ownerAccount: pdf.data.titular_cuenta,
        account: pdf.data.Cuenta,
        customerEmail: pdf.data.Correo_electronico,
        batch: pdf.data.batch
      })
    })
    .catch(error => {
      process.send(error)
    })
})