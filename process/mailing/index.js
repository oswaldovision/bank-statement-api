const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env['sendGridApiKey'])

const mailing = {
  sendMail (settings) {
    const mail = {
      to: settings.to,
      from: process.env['MAIL_FROM'],
      subject: settings.subject,
      html: '<p><span><b>Correo de prueba.</b></span></p>Enviado usando Azure sendGrid services<p>Adjunto extracto</p> <span>Por favor no responda este mensaje</span>',
      attachments: settings.attachments
    }

    sgMail.send(mail)
      .then(() => {
        console.log(`mail send with attachment to: ${settings.to.join(' ')}`)
      })
      .catch(error => {
        const {message, code, response} = error
        console.log('Fail mailing: ' + message + response)
      })
  }
}

module.exports = mailing