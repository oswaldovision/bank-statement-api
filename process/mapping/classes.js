const crypto = require('crypto')

class Product {
  constructor (header) {
    this.header = header
    this.details = []
  }
}

class Statement {
  constructor (codeClient, batch) {
    this.codeClient = codeClient
    this.nameClient = ''
    this.batch = batch
  }
}

module.exports =
  {
    Product,
    Statement
  }