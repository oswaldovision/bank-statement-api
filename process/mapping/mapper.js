const {randomBytes} = require('crypto')
const {createInterface} = require('readline')
const {createReadStream, writeFile, appendFileSync, unlink} = require('fs')
const {buildTemplateProduct, tc, pac, csm} = require('./mapProducts')
const {Product, Statement} = require('./classes')
const {insertData} = require('../../db/statement')

const buildStatement = (line, statement) => {
  if (line.startsWith('NOMBRE CLIENTE:')) {
    statement.nameClient = line.split('NOMBRE CLIENTE:')[1]
  } else if (line.startsWith(process.env['CSM_HEADER'])) {
    statement.csm = mapHeader(line.split(/:(.+)/)[1], csm.header)
  } else if (line.startsWith(process.env['CSM_DETAIL'])) {
    statement.csm.details.push(mapDetail(line.split(/:(.+)/)[1], csm.detail))
  } else if (line.startsWith(process.env['TC_HEADER'])) {
    statement.tc = mapHeader(line.split(/:(.+)/)[1], tc.header)
  } else if (line.startsWith(process.env['TC_DETAIL'])) {
    statement.tc.details.push(mapDetail(line.split(/_(.+)/)[1], tc.detail))
  } else if (line.startsWith(process.env['PAC_HEADER'])) {
    statement.pac = mapHeader(line.split(/:(.+)/)[1], pac.header)
  } else if (line.startsWith(process.env['PAC_DETAIL'])) {
    statement.pac.details.push(mapDetail(line.split(/:(.+)/)[1], pac.detail))
  }
}

const mapHeader = (values, type) => {
  let header = buildTemplateProduct(values, type)
  return new Product(header)
}

const mapDetail = (values, type) => {
  return buildTemplateProduct(values, type)
}

const createFileOnDisk = (data) => {
  const client = data.nameClient ? data.nameClient : 'No Name'
  writeFile(`./output/${client}-${randomBytes(16).toString('hex')}.json`,
    JSON.stringify(data),
    'utf8',
    (err) => {
      if (err) throw err

      // success case, the file was saved
      console.log(client + '.json   --> file saved!')
    })
}

const processMap = (sourceFile) => {

  appendFileSync(sourceFile, `\n${process.env['LAST_LINE']}`)

  let statement = {}
  const batch = randomBytes(16).toString('hex')

  const lineReader = createInterface({
    input: createReadStream(sourceFile),
  })

  lineReader.on('line', function (line) {
    if (line) {
      if (line.startsWith(process.env['NEW_CLIENT'])) {
        if (statement.codeClient) {
          // createFileOnDisk(statement)
          insertData(statement)
        }
        statement = new Statement(line.split(process.env['NEW_CLIENT'])[1], batch)
      } else if (line.trim() === process.env['LAST_LINE']) {
        // createFileOnDisk(statement)
        insertData(statement)
        unlink(sourceFile, (err) => {
          if (err) {
            console.error(err)
            return
          }
          console.log('File deleted !')
        })
        return
      } else {
        buildStatement(line, statement)
      }
    }
  })

  return batch
}

module.exports = {processMap}