const mapDataCsm = (data) => {
  return {
    batch: data.batch,
    tipo_producto: 'Consumo',
    titular_cuenta: data.nameClient,
    direccion_fisica: data.csm.header.Direccion,
    ciudad: data.csm.header.Ciudad,
    numero_cuenta: data.csm.header.Numero_Cuenta,
    numero_paginas: data.csm.header.Numero_Paginas,
    fecha_generacion: data.csm.header.Fecha_Fact,
    fecha_facturacion: data.csm.header.Fecha_Facturacion,
    fecha_limite: data.csm.header.Fecha_Limite_de_Pago,
    Detalles: data.csm.details,
    linea_credito: data.csm.header.Linea_Credito,
    valor_desembolsado: data.csm.header.Valor_Utilizado,// TODO: Preguntar si corresponde
    desembolso_mes: data.csm.header.Pago_Total_Mes,// TODO: Preguntar si corresponde,
    abono_capital: data.csm.header.Abono_Capital_Anterior,
    interes_corriente: data.csm.header.Intereses_Corrientes,//TODO: ??,
    interes_mora: data.csm.header.Intereses_Mora, // TODO: 3 ??,
    prima_seguros: data.csm.header.Seguro,// TODO: Preguntar si corresponde,
    prima_desempleo: data.csm.header.Seguro_Desempleo,
    gastos_cobranza: data.csm.header.Gastos_de_Cobranza,
    otros: data.csm.header.Otros_Cargos,
    total_pagado: data.csm.header.Total_Recaudo,// TODO: Preguntar si corresponde,
    saldo_mora: data.csm.header.Saldo_Final,
    abono_capital_prox: data.csm.header.Capital,
    interes_corriente_prox: data.csm.header.Intereses_Corrientes,// TODO: ??,
    interes_mora_prox: data.csm.header.Interes_Mora_Anterior,// TODO: 4 ??,
    prima_seguros_prox: data.csm.header.Seguros_pagado,
    prima_desempleo_prox: data.csm.header.Seguro_Desempleo_2,
    otros_prox: data.csm.header.Otros_Cargos_Mes_Anterior,
    total_pagar: data.csm.header.Pago_Total_Mes,
    saldo_total_fecha: data.csm.header.Saldo_Final,
    interes_corriente_EA: data.csm.header.Interes_Corriente_Anterior,
    tasa_mora_EA: data.csm.header.Tasa_Mora_EA,
    dias_mora: data.csm.header.Dias_Mora,
    Correo_electronico: data.csm.header.Correo_electronico,
    Cuenta: data.csm.header.Cuenta,
    footerposition: 5
  }
}

const mapDataTc = (data) => {
  return {
    batch: data.batch,
    titular_cuenta: data.nameClient,
    tipo_producto: 'TC',
    direccion_fisica: data.tc.header.Direccion,
    ciudad: data.tc.header.Ciudad,
    tarjeta_cmr: data.tc.header.Cuenta.substr(data.tc.header.Cuenta.length - 4),
    fecha_facturacion: data.tc.header.Fecha_Facturacion,
    fecha_limite: data.tc.header.Fecha_Limite_Pago,
    Detalles: data.tc.details,
    saldo_anterior: data.tc.header.Saldo_anterior,
    consumo_mes: data.tc.header.Consumo_del_mes,
    intereses_moraST: data.tc.header.Intereses_mora, //TODO:?? interes_mora 1
    intereses_corrientesST: data.tc.header.Intereses_corriente, //TODO:?? Interes_corriente 1
    avances_mes: data.tc.header.Cargo_avances, //TODO: ?? cargo avances 1
    otros_cargos: data.tc.header.otros_cargos_Saldo_Total,// TODO:??
    pagos_y_creditos: data.tc.header.Pagos_y_Creditos,
    saldo_al_corte: data.tc.header.Saldo_anterior,
    pac_cuenta_ahorro: data.tc.header.Cuota_de_ahorro_pac,
    pago_total_corte: data.tc.header.Pago_Total_del_Mes,
    saldo_en_mora: data.tc.header.Saldo_mora,
    cuotas_consumo: data.tc.header.Cuotas_consumo,
    intereses_moraPM: data.tc.header.intereses_mora,//TODO:?? interes_mora 2
    intereses_corrientesPM: data.tc.header.Intereses_corriente,//TODO:?? Interes_corriente 2
    cuota_avances: data.tc.header.Cargo_avances,
    otros_cargosPM: data.tc.header.otros_cargos_pago_minimo,
    pague_hasta: data.tc.header.Fecha_Limite_Pago,
    pago_minimoPM: data.tc.header.Pago_Total_del_Mes,
    PACCta_Ahorro: data.tc.header.Cuota_de_ahorro_pac,
    pago_total_mes: data.tc.header.cuota_mes,
    cupo_cmr: data.tc.header.Cupo_Tarjeta,
    disponible_cmr: data.tc.header.Disponible_Tarjeta,
    disponible_avances: data.tc.header.Disponible_avances,
    saldo_capital: data.tc.header.Capital,
    tasa_avances: data.tc.header.Tasa_de_Avance,
    tasa_avance_EA: data.tc.header.tasa_efectiva_avance,
    tasa_compras: data.tc.header.Tasa_de_Interes,
    tasa_compra_EA: data.tc.header.tasa_efectiva_compra,
    tasa_mora: data.tc.header.Tasa_de_Mora,
    tasa_mora_EA: data.tc.header.tasa_efectiva_mora,
    interes_corriente: data.tc.header.Intereses_corriente,
    gastos_cobranza: data.tc.header.Gastos_de_Cobranza,
    capital_avance: data.tc.header.cuota_de_avance,
    cuota_manejo: data.tc.header.Cuota_de_manejo,
    interes_mora: data.tc.header.Intereses_mora,
    interes_corriente_ant: data.tc.header.Intereses_Corrientes,
    capital: data.tc.header.Capital,
    pac_ahorro_ant: data.tc.header.Cuota_de_ahorro_pac,
    total_pagado: data.tc.header.Total_pagos,
    fecha_pago: data.tc.header.Fecha_Limite_Pago,
    pago_minimo: data.tc.header.Pago_minimo_antes_PAC,
    pac_ahorro: data.tc.header.Cuota_de_ahorro_pac,
    pago_minimo_pac: data.tc.header.Pago_minimo_antes_PAC,
    dias_mora: data.tc.header.dias_de_mora,
    Correo_electronico: data.tc.header.Correo_electronico,
    Cuenta: data.tc.header.Cuenta,
    numero_paginas: '2'
  }
}

const mapDataPac = (data) => {
  return {
    batch: data.batch,
    tipo_producto: 'PAC',
    titular_cuenta: data.nameClient,
    direccion_fisica: '',
    ciudad: '',
    numero_cuenta: data.pac.header.CUENTA,
    fecha_desde: data.pac.header.FECHA_FACTURACION,
    fecha_hasta: data.pac.header.FECHA_FACTURACION,
    numero_paginas: 3,
    fecha_generacion: data.pac.header.FECHA_FACTURACION,
    Detalles: data.pac.details,
    saldo_anterior: data.pac.header.SALDO_ANTERIOR,
    total_debitos: '',
    total_creditos: '',
    gravamen: '',
    retefuente: '',
    saldo_canje: '',
    saldo_final: ''
  }
}

module.exports = {
  mapDataPac,
  mapDataCsm,
  mapDataTc
}