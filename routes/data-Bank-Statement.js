const router = require('express').Router()
const db = require('./../db/db')

router.get('/:id', (req, res) => {
  db.get().collection('statements').find({'batch': req.params.id}).toArray((err, result) => {
    if (err) throw err

    res.send(result)
    // db.close(() => console.log('Close db'))
  })
})

module.exports = router