const router = require('express').Router()
const {mapDataCsm, mapDataTc, mapDataPac} = require('../process/mapping/mapperToHtml')
const {getImages, downloadStatementById} = require('./../db/statement')
const {buildSaveMailing, tcForked, csmForked, pacForked} = require('./../process/fileBuilder/engineFile')

router.get('/getImages', (req, res) => {
  getImages(req, res)
})

router.get('/downloadStatementById/:id', (req, res) => {
  downloadStatementById(req, res)
})

router.post('/create', (req, res) => {

  if (req.body.hasOwnProperty('csm')) {
    buildSaveMailing(req.body, process.env['TEMPLATE_CSM'], mapDataCsm, csmForked)
  }

  if (req.body.hasOwnProperty('tc')) {
    buildSaveMailing(req.body, process.env['TEMPLATE_TC'], mapDataTc, tcForked)
  }

  if (req.body.hasOwnProperty('pac')) {
    buildSaveMailing(req.body, process.env['TEMPLATE_PAC'], mapDataPac, pacForked)
  }

  return res.send('OK')

})

module.exports = router