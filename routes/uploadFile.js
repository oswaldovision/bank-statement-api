const router = require('express').Router()
const {processMap} = require('../process/mapping/mapper')

router.post('/upload', (req, res) => {
  const file = req.file
  if (!file) {
    const error = new Error('Please upload a file')
    error.httpStatusCode = 400
    return next(error)
  }
  const batch = processMap(`./${file.path}`)
  res.send({file, batch})
})

module.exports = router